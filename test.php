<?php

/*
    This is the FDF field structure
        <</V (fName) /T (first_name)>> 
        <</V (lName) /T (last_name)>>
    also can be
        <</T (lName) /V (last_name)>>
*/

/* 
    TODO :::::: wOwO!
    :: Needs to upload a PDF file first.
    :: Could add other method to return all the form field names. 
 */

// Form data:
$formFields['1-1'] = 'Tom';
$formFields['1-2'] = 'Smith';
$formFields['1-3'] = '8978564';
$formFields['1-4'] = '08/07/2011';
$formFields['1-5'] = 'male';

$proccessPdf = new ProccessPdf();
$proccessPdf->startProccess($formFields, 'claim-form.pdf', 'claim-form'); //Fields, PDF File Path, Output file name

class ProccessPdf {

    //public $formFields;
    public function startProccess($pdfFields, $pdfSourcePath, $outputName){
        
        $fdfFields = null;

        //Construct the FDF Fields.
        foreach ($pdfFields as $key => $value) {
            $fdfFields .= "<</T (" . $key . ") /V (" . $value . ")>>";
        }
        //die(var_dump($fdfFields)); //<-- to see what the construct looks like.

        $fdfConstruct = $this->_getHead() . $fdfFields . $this->_getFoot();

        // Creating a temporary file for our FDF file.
        $FDFfile = tempnam(sys_get_temp_dir(), gethostname());

        file_put_contents($FDFfile, $fdfConstruct);

        /* Merging the FDF file with the raw PDF form,
         * Keep an eye ball on the spacing before and after the concatenation!
         */

        $execString = "pdftk " . $pdfSourcePath . " fill_form $FDFfile output " . $outputName . "_output.pdf";
        //die(var_dump($execString)); //<-- to see what the execString looks like.

        exec($execString);
        // Removing the FDF file as we don't need it any more
        unlink($FDFfile);

    }

    private function _getHead(){

        $fdfHead = 
        "%FDF-1.2
        %<E2><E3><CF><D3>
        1 0 obj 
        <<
        /FDF 
        <<
        /Fields [";

        return $fdfHead;
    }

    private function _getFoot(){

        $fdfFoot = "]
        >>
        >>
        endobj 
        trailer
        <<
        /Root 1 0 R
        >>
        %%EOF";

        return $fdfFoot;
    }

}
